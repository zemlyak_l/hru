package main

import (
	"bufio"
	"os"
	"strings"
	"unicode"
)

const BUFSIZE = 1 << 19

func main() {
	r := bufio.NewReaderSize(os.Stdin, BUFSIZE)
	w := bufio.NewWriterSize(os.Stdout, BUFSIZE)

	i := 0

	for {
		c, _, err := r.ReadRune()
		if err != nil {
			break
		}

		if isLetter(c) {
			switch i {
			case 0:
				w.WriteRune('х')
				i = 1

			case 1:
				w.WriteRune('р')
				i = 2

			default:
				w.WriteRune('ю')

			}
		} else {
			i = 0
			w.WriteRune(c)
		}
	}

	w.Flush()
}

func isLetter(c rune) bool {
	return  'a' <= c && c <= 'z' ||
		'A' <= c && c <= 'Z' ||
		'а' <= c && c <= 'я' ||
		'А' <= c && c <= 'Я' ||

		c >= 0x80 && unicode.IsLetter(c)

}

func Hru(inp string) (out string) {
	b := &strings.Builder{}
	b.Grow(len(inp))

	i := 0
	for _, c := range inp {
		if isLetter(c) {
			switch i {
			case 0:
				b.WriteRune('х')
				i = 1

			case 1:
				b.WriteRune('р')
				i = 2

			default:
				b.WriteRune('ю')

			}
		} else {
			i = 0
			b.WriteRune(c)
		}
	}

	return b.String()
}
