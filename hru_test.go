package main

import (
	"testing"
)

const datastr  = "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~';АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя"

const expected = "!\"#$%&'()*+,-./0123456789:;<=>?@хрюююююююююююююююююююююююю[\\]^_`хрюююююююююююююююююююююююю{|}~';хрюююююююююююююююююююююююююююююююююююююююююююююююююююююююююююююююю"

func TestHru(t *testing.T) {
	out := Hru(datastr)

	assert(t, out, expected)

	return
}

func assert(t *testing.T, x string, y string) {
	if x != y {
		t.Log("автор еблан")
		t.FailNow()
	}
}
